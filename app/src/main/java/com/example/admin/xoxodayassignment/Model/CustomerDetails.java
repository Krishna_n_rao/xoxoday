package com.example.admin.xoxodayassignment.Model;

/**
 * Created by admin on 04-07-2018.
 */

public class CustomerDetails {

    String custName,custCheck;


    public String getCustCheck() {
        return custCheck;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustCheck(String custCheck) {
        this.custCheck = custCheck;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }
}
