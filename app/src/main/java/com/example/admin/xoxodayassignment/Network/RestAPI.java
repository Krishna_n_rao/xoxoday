package com.example.admin.xoxodayassignment.Network;

import com.google.gson.JsonObject;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by admin on 03-07-2018.
 */

public interface RestAPI {

    @GET("AndroidXoxoday")
    Observable<JsonObject> getData();
}
