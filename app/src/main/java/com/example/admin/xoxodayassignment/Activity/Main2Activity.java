package com.example.admin.xoxodayassignment.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.xoxodayassignment.Model.CustomerDetails;
import com.example.admin.xoxodayassignment.R;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main2Activity extends AppCompatActivity {

    JSONArray custDetails;
    String totalSelected;
    LinearLayout custListLayout;
    ArrayList<String> details = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        custListLayout = (LinearLayout) findViewById(R.id.custListLayout);
        Bundle b = getIntent().getExtras();

        if (b != null) {
            try {
                //selectedCustomer.setText(b.getString("customerName").replaceAll("[\\[\\](){}]",""));
                custDetails = new JSONArray(b.getString("customerName"));
                for (int i = 0; i < custDetails.length(); i++) {
                  details.add(custDetails.get(i).toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (details.size() > 0) {
                Collections.sort(details);
                for (int i = 0; i < details.size(); i++) {
                    LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    TextView tv = new TextView(this);
                    tv.setLayoutParams(lparams);
                    tv.setPadding(5,5,5,5);
                    tv.setText((i + 1) + ") " + details.get(i).toString());
                    this.custListLayout.addView(tv);
                     }
            }

            Log.e("app", "onCreate:MainActivity2 " + b.getString("customerName"));
            Log.e("app", "onCreate:MainActivity2  totalSelected " + totalSelected);
        }
        else{
            LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView tv = new TextView(this);
            tv.setLayoutParams(lparams);
            tv.setPadding(5,5,5,5);
            tv.setText("Oh no one invited yet");
            this.custListLayout.addView(tv);
        }


    }
}
