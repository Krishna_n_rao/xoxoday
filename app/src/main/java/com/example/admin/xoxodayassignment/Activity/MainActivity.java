package com.example.admin.xoxodayassignment.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;


import com.example.admin.xoxodayassignment.Model.CustomerDetails;
import com.example.admin.xoxodayassignment.Network.NetworkRequest;
import com.example.admin.xoxodayassignment.Network.RestAPI;
import com.example.admin.xoxodayassignment.Network.RestAPIBuilder;
import com.example.admin.xoxodayassignment.R;
import com.example.admin.xoxodayassignment.Reciever.ConnectivityReceiver;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import rx.Subscription;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RestAPI api;
    private Subscription mNetworkSubscription;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;
    private List<CustomerDetails> details = new ArrayList<>();
    CustomerDataAdapter customerDataAdapter;
    RecyclerView recyclerView;
    Button selectedItem;
    Context mContext;
    List<String> jsonValues = new ArrayList<String>();
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Boolean isConnected = ConnectivityReceiver.isConnected(this);
        mContext = this;
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        selectedItem = (Button) findViewById(R.id.selectedItem);
        if (isConnected) {
            try {
                pd = new ProgressDialog(this);
                pd.setTitle("Loading...");
                pd.setCancelable(false);
                pd.show();
                api = RestAPIBuilder.buildRetrofitService();
                mNetworkSubscription = NetworkRequest.performAsyncRequest(api.getData(), (data) -> {
                    // Update UI on main thread
                    try {
                       for (int i = 0; i < data.getAsJsonArray("data").size(); i++) {
                            CustomerDetails customerDetails = new CustomerDetails();
                            customerDetails.setCustName(data.getAsJsonArray("data").get(i).getAsJsonObject().get("name").toString().replaceAll("^\"|\"$", ""));
                            customerDetails.setCustCheck(data.getAsJsonArray("data").get(i).getAsJsonObject().get("isSelcted").toString());
                            details.add(customerDetails);
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setAdapter();
                            }
                        });
                    } catch (Exception e) {
                        Log.e("App", "Exception " + e.toString());

                    } finally {
                        Log.e("App", "exception: status Finished" + STATUS_FINISHED);
                    }
                }, (error) -> {
                    // Handle Error
                    Log.e("App", "login Error: " + error.toString() + "Status Error" + STATUS_ERROR);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mContext, error.toString(), Toast.LENGTH_SHORT).show();
                            if(pd.isShowing()){
                                pd.dismiss();
                            }
                        }
                    });


                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {

            Snackbar snackbar = Snackbar.make(findViewById(R.id.mainContainter), "Please Check internet connection", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
        selectedItem.setOnClickListener(this);
    }

    public void setAdapter() {

        Collections.sort(details, new Comparator<CustomerDetails>() {
            @Override
            public int compare(CustomerDetails customerDetails, CustomerDetails t1) {
                return customerDetails.getCustName().compareTo(t1.getCustName());
            }
        });
        customerDataAdapter = new CustomerDataAdapter(mContext, details);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(customerDataAdapter);
        if(pd.isShowing()){
            pd.dismiss();
        }

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.selectedItem) {
           if(jsonValues.size()>0){
               Intent next = new Intent(this, Main2Activity.class);
               next.putExtra("customerName", jsonValues.toString());
               startActivity(next);
           }
           else{
               Toast.makeText(mContext, "You have not selected any!!", Toast.LENGTH_SHORT).show();
           }

        }
    }

    //Customer Details Adapter Class
    class CustomerDataAdapter extends RecyclerView.Adapter<CustomerDataAdapter.CustomerViewHolder> {
        private List<CustomerDetails> dataDetails = Collections.emptyList();
        CustomerDetails customerDetails;
        Context mContext;


        public CustomerDataAdapter(Context context, List<CustomerDetails> dataDetails) {
            this.dataDetails = dataDetails;
            mContext = context;
            Log.e("App", "CustomerDataAdapter: " + dataDetails);
        }

        @Override
        public CustomerDataAdapter.CustomerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customrecyclerview, parent, false);
            return new CustomerViewHolder(view);
        }

        @Override
        public void onBindViewHolder(CustomerDataAdapter.CustomerViewHolder holder, int position) {
            CustomerDataAdapter.CustomerViewHolder customerViewHolder = (CustomerDataAdapter.CustomerViewHolder) holder;

            CustomerDetails current = dataDetails.get(position);
            holder.custName.setText(current.getCustName());

            holder.custCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Log.e("app", "onCheckedChanged: " + current.getCustName());
                    if (!b) {
                        Log.e("app", "onCheckedChanged: and !b " + current.getCustName());
                        if (jsonValues.contains(current.getCustName())) {
                            jsonValues.remove(current.getCustName());
                        }
                    } else {
                        if (!jsonValues.contains(current.getCustName())) {
                            jsonValues.add(current.getCustName());
                        }
                        Log.e("app", "onCheckedChanged: and b " + current.getCustName());
                    }

                    /*Intent nextActivity = new Intent(mContext,Main2Activity.class);
                    nextActivity.putExtra("customerName",current.getCustName());
                    mContext.startActivity(nextActivity);*/


                }
            });

        }

        @Override
        public int getItemCount() {
            return dataDetails.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public class CustomerViewHolder extends RecyclerView.ViewHolder {
            public CheckBox custCheck;
            public TextView custName;


            public CustomerViewHolder(View view) {
                super(view);
                custName = (TextView) view.findViewById(R.id.nameText);
                custCheck = (CheckBox) view.findViewById(R.id.selectedCheck);

            }


        }
    }
}
